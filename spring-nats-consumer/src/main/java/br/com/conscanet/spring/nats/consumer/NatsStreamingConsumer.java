package br.com.conscanet.spring.nats.consumer;

import java.io.IOException;
import java.util.concurrent.TimeoutException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import io.nats.streaming.Message;
import io.nats.streaming.MessageHandler;
import io.nats.streaming.StreamingConnection;
import io.nats.streaming.Subscription;
import io.nats.streaming.SubscriptionOptions;

@Configuration
public class NatsStreamingConsumer {
	
	private Logger log = LoggerFactory.getLogger(NatsStreamingConsumer.class);
	
	@Value("${consumer.sleepTime}")
	private int sleepTime;

	@Bean(destroyMethod = "close")
	public Subscription subscription(StreamingConnection stan) throws IOException, InterruptedException, TimeoutException {
		Subscription sub = stan.subscribe("msgs", new MessageHandler() {
		    public void onMessage(Message m) {
		    	log.debug("[{}] Message received", Thread.currentThread().getName());
		    	try {
					Thread.sleep(sleepTime);
					m.ack();
				} catch (InterruptedException | IOException e) {
					log.error("[{}] Error receiving message: {}", Thread.currentThread().getName(), e.getMessage(), e);
				}
		    }
		}, new SubscriptionOptions.Builder().manualAcks().build());
		
		return sub;
	}
}
