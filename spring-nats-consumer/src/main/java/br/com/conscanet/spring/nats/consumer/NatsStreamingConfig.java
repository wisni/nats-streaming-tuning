package br.com.conscanet.spring.nats.consumer;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import io.nats.client.Connection;
import io.nats.client.Nats;
import io.nats.client.Options;
import io.nats.streaming.NatsStreaming;
import io.nats.streaming.StreamingConnection;

@Configuration
public class NatsStreamingConfig {

	@Value("${nats.url}")
	private String natsURL;
		
	@Bean(destroyMethod = "close")
	public Connection nats() throws Exception {
		Options.Builder builder = new Options.Builder();
		builder.server(this.natsURL);

		return Nats.connect(builder.build());
	}

	@Bean(destroyMethod = "close")
	public StreamingConnection natsStreaming(Connection nats) throws Exception {
		io.nats.streaming.Options.Builder builder = new io.nats.streaming.Options.Builder();
		builder.natsConn(nats);

		return NatsStreaming.connect("test-cluster", "consumer", builder.build());
	}
}
