package br.com.conscanet.spring.nats.producer;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringNatsProducerApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringNatsProducerApplication.class, args);
	}

}
