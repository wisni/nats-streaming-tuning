package br.com.conscanet.spring.nats.producer;

import java.io.IOException;
import java.util.Random;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeoutException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import io.nats.streaming.StreamingConnection;

@RestController
@RequestMapping(value = "/msgs")
public class NatsProducer {
	
	private Logger log = LoggerFactory.getLogger(NatsProducer.class);

	private StreamingConnection stan;

	public NatsProducer(StreamingConnection stan) {
		this.stan = stan;
	}

	@GetMapping
	public ResponseEntity<String> send(@RequestParam int n, @RequestParam int c, @RequestParam int s)
			throws IOException, InterruptedException, TimeoutException {
		
		ExecutorService executor = this.getExecutor(c);
		
		byte[] msg = this.generateMsg(s);
		
		for (int i = 1; i <= n / c; i++) {
			this.sendConcurrent(c, executor, msg);
		}
		
		return ResponseEntity.ok(String.format("%d messages sent.", n));
	}
	
	private void sendConcurrent(int c, ExecutorService executor, byte[] msg) {
		for (int i = 1; i <= c; i++) {
			executor.submit(() -> {
				try {
					stan.publish("msgs", msg);
					this.log.debug("[{}] Message sent", Thread.currentThread().getName());
				} catch (IOException | InterruptedException | TimeoutException e) {
					this.log.error("[{}] Error sending message: {}", Thread.currentThread().getName(), e.getMessage(), e);
				}
			});			
		}
	}
	
	private byte[] generateMsg(int size) {
		Random random = new Random();
		byte[] b = new byte[size];
		random.nextBytes(b);
		
		return b;
	}
    
	private ExecutorService getExecutor(int concurrent) {
        return Executors.newFixedThreadPool(concurrent);
    }
}
